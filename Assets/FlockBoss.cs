﻿using System.Linq;
using UnityEngine;
using System;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class FlockBoss : MonoBehaviour
{
    public float maxVelocity = 3f;
    public float maxForce = 10f;

    public float separationWeight = 1f;
    public float alignmentWeight = 1f;
    public float cohesionWeight = 1f;
    public float wanderWeight = 1f;

    public bool playerControl = false;

    Rigidbody2D rb;
    Transform spriteTransform;

    float dumbPerlinOffset;


    HashSet<Rigidbody2D> pigNeighbours = new HashSet<Rigidbody2D>();

    //TODO: chop into player control, sprite flipping
    void Awake()
    {
        dumbPerlinOffset = Random.Range(-10000f, 0);

        rb = GetComponent<Rigidbody2D>();
        spriteTransform = transform.Find("Sprite");
        Assert.That(() => rb != null && spriteTransform != null);
    }
    
    void FixedUpdate()
    {
        if(playerControl) {
            var input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")).normalized;
            rb.velocity = input * maxVelocity;
        }
        else {
            var force = new Vector2();
            force += Separation(pigNeighbours) * separationWeight;
            force += Alignment(pigNeighbours) * alignmentWeight;
            force += Cohesion(pigNeighbours) * cohesionWeight;
            force += DumbPerlinWander() * wanderWeight;
            if(force.sqrMagnitude > maxForce * maxForce) {
                force = force.normalized * maxForce;
            }
            rb.AddForce(force, ForceMode2D.Force);
            Debug.DrawRay(transform.position, force, Color.green);
        }
    }

    void LateUpdate()
    {
        var xv = rb.velocity.x;
        if(xv < -0.01f) {
            spriteTransform.localScale = new Vector3(1f, 1f, 1f);
        }
        else if(xv > 0.01f) {
            spriteTransform.localScale = new Vector3(-1f, 1f, 1f);            
        }
        
    }

    Vector2 Separation(IEnumerable<Rigidbody2D> neighbours)
    {
        var p = rb.position;
        
        //TODO: is there a more efficient way to calculate the force instead of normalizing and getting the magnitude?
        return neighbours
            .Select(n => p - n.position)
            .Aggregate(new Vector2(), (current, toAgent) => current + toAgent.normalized / toAgent.magnitude);
    }

    Vector2 Alignment(IEnumerable<Rigidbody2D> neighbours)
    {
        if(!neighbours.Any()) {
            return Vector2.zero;
        }

        return neighbours
            .Select(n => n.GetComponent<Rigidbody2D>().velocity.normalized)
            .Aggregate(new Vector2(), (current, v) => current + v)
            / neighbours.Count();
    }

    Vector2 Cohesion(IEnumerable<Rigidbody2D> neighbours)
    {
        if(!neighbours.Any()) {
            return Vector2.zero;
        }

        var centerOfMass = neighbours
            .Select(n => n.position)
            .Aggregate(new Vector2(), (current, v) => current + v)
            / neighbours.Count();

        return Seek(centerOfMass);
    }

    Vector2 Seek(Vector2 target)
    {
        var desiredVelocity = (target - transform.position.xy()).normalized * maxVelocity;
        return desiredVelocity - rb.velocity;
    }

    Vector2 DumbPerlinWander()
    {
        return new Vector2(Mathf.PerlinNoise(Time.time, dumbPerlinOffset) * 2f - 1f, Mathf.PerlinNoise(dumbPerlinOffset, Time.time) * 2f - 1f);
    }

    Vector2 WallAvoidance()
    {
        throw new NotImplementedException();
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        var orb = other.GetComponent<Rigidbody2D>();

        if(orb == null) {
            return;
        }

        //Debug.Log(name + ": added " + orb.name, this);

        //TODO: add to correct group...
        pigNeighbours.Add(orb);

    }

    void OnTriggerExit2D(Collider2D other)
    {
        var orb = other.GetComponent<Rigidbody2D>();

        if(orb == null) {
            return;
        }

        //Debug.Log(name + ": removing " + orb.name, this);

        pigNeighbours.Remove(orb);
    }

}
